﻿using System;
using listaStrings = System.Collections.Generic.List<string>;
using listaintegers = System.Collections.Generic.List<int>;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            // Arrays
            string[] nombres = {"Marcos", "Diego", "Nicolas", "Javier"};

            string[] variable = { "Variable1", "Variable2" };

            nombres[0] = "Allan";

            int[] notas = { 1, 2, 3, 4, 5 , 6 , 7};

            string[] juegos = { "Pacman", "ajedrez", "Mario Bros", "Donkey Kong" };

            // ImprimirArray(juegos);

            listaStrings juegosMenos1 = RemoverElemento(juegos, "ajedrez");
            ImprimirLista(juegosMenos1);
            
            listaintegers notasmenos1 = RemoverElemento(notas, 1);
            // notasmenos1.ForEach(Console.WriteLine);
            ImprimirLista(notasmenos1);

            // Ordenamiento de arrays
            // Array.Sort(nombres);

            // ImprimirArray(nombres);
            // ImprimirArray(variable);

            //ImprimirArray(notas);

            Console.WriteLine();
            int result = Suma(1 , 2);
            Console.WriteLine(result);

            Double result2 = Suma(3.1 , 3.1);
            Console.WriteLine(result2);

            
        }
        static void ImprimirArray(string[] _parametro) 
        {
            foreach(string i in _parametro)
            {
                // Console.WriteLine();
                Console.WriteLine(i);
            }
        }

        static void ImprimirArray(int[] _parametro)
        {
            foreach(int i in _parametro)
            {
                Console.WriteLine(i);
            }
        }

        // Crear un metodo que permita eliminar un elmento de un array
        static listaStrings RemoverElemento(string[] _arreglo, string _nombre)
        {
            listaStrings elementos = new listaStrings(_arreglo);
            elementos.Remove(_nombre);
            // elementos.ForEach(System.Console.WriteLine);
            return elementos;
        }

        static listaintegers RemoverElemento(int[] _arreglo, int valor)
        {
            listaintegers elementos = new listaintegers(_arreglo);
            elementos.Remove(valor);
            return elementos;
        } 

        static int Suma(int a, int b)
        {
            return a + b;
        }

        static Double Suma(Double a, Double b)
        {
            return a + b;
        }

        static void ImprimirLista(listaintegers lista)
        {
            lista.ForEach(Console.WriteLine);
        }

        static void ImprimirLista(listaStrings lista)
        {
            lista.ForEach(Console.WriteLine);
        }
    }
}
